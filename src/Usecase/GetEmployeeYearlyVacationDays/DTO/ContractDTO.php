<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO;

use DateTimeImmutable;
use Ottonova\Infrastructure\Exceptions\InvalidContractStartDayException;

class ContractDTO
{
    const VALID_START_DAYS = [1, 15];

    private DateTimeImmutable $startDate;
    private ?int $specialContractVacationDays;
    private int $startYear;
    private int $startMonth;
    private int $startDay;

    /**
     * @param DateTimeImmutable $startDate
     * @param int|null $specialContractVacationDays
     * @throws InvalidContractStartDayException
     */
    public function __construct(DateTimeImmutable $startDate, ?int $specialContractVacationDays)
    {
        $this->setStartDate($startDate);
        $this->specialContractVacationDays = $specialContractVacationDays;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getStartDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @return int
     */
    public function getStartYear(): int
    {
        return $this->startYear;
    }

    /**
     * @return int
     */
    public function getStartMonth(): int
    {
        return $this->startMonth;
    }

    /**
     * @return int
     */
    public function getStartDay(): int
    {
        return $this->startDay;
    }

    /**
     * @return int|null
     */
    public function getSpecialContractVacationDays(): ?int
    {
        return $this->specialContractVacationDays;
    }

    /**
     * @param DateTimeImmutable $startDate
     * @throws InvalidContractStartDayException
     */
    private function setStartDate(DateTimeImmutable $startDate)
    {
        $startDay = (int)$startDate->format('d');
        if (!in_array($startDay, self::VALID_START_DAYS)) {
            throw new InvalidContractStartDayException(trans('exceptions.invalidContractStartDay'));
        }
        $this->startDate = $startDate;
        $this->startYear = (int)$startDate->format('Y');
        $this->startMonth = (int)$startDate->format('m');
        $this->startDay = (int)$startDate->format('d');
    }
}
