<?php

namespace Ottonova\Infrastructure\Persistence\Repository\Employee;

class EmployeeContractsRepository implements EmployeeContractsRepositoryInterface
{
    private array $employeesList = [
        [
            'name' => "Hans Müller",
            'birth_date' => '30.12.1950',
            'contract_start_date' => '01.01.2001',
            'special_contract_vacation_days' => null,
        ],
        [
            'name' => "Angelika Fringe",
            'birth_date' => '09.06.1966',
            'contract_start_date' => '15.01.2001',
            'special_contract_vacation_days' => null,
        ],
        [
            'name' => "Peter Klever",
            'birth_date' => '12.07.1991',
            'contract_start_date' => '15.05.2016',
            'special_contract_vacation_days' => 27,
        ],
        [
            'name' => "Marina Helter",
            'birth_date' => '26.01.1970',
            'contract_start_date' => '15.01.2018',
            'special_contract_vacation_days' => null,
        ],
        [
            'name' => "Marina Helter",
            'birth_date' => '26.01.1970',
            'contract_start_date' => '15.01.2018',
            'special_contract_vacation_days' => null,
        ]
    ];

    public function getAllEmployees(): array
    {
        return $this->employeesList;
    }
}
