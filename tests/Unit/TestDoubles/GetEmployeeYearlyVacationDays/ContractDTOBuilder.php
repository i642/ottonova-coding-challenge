<?php

namespace App\Unit\TestDoubles\GetEmployeeYearlyVacationDays;

use DateTimeImmutable;
use Ottonova\Infrastructure\Exceptions\InvalidContractStartDayException;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\ContractDTO;

class ContractDTOBuilder
{
    private DateTimeImmutable $startDate;
    private ?int $specialContractVacationDays;

    public function __construct()
    {
        $this->startDate = new DateTimeImmutable('01.05.2009');
        $this->specialContractVacationDays = null;
    }

    /**
     * @param string $startDate
     * @return $this
     * @throws \Exception
     */
    public function withStartDate(string $startDate): self
    {
        $this->startDate = new DateTimeImmutable($startDate);
        return $this;
    }

    public function withSpecialContractVacationDays(?int $specialContractVacationDays): self
    {
        $this->specialContractVacationDays = $specialContractVacationDays;
        return $this;
    }


    /**
     * @return ContractDTO
     * @throws InvalidContractStartDayException
     */
    public function build(): ContractDTO
    {
        return new ContractDTO($this->startDate, $this->specialContractVacationDays);
    }
}
