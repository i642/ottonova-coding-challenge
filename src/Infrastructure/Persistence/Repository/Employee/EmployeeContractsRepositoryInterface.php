<?php

namespace Ottonova\Infrastructure\Persistence\Repository\Employee;

interface EmployeeContractsRepositoryInterface
{
    public function getAllEmployees(): array;
}
