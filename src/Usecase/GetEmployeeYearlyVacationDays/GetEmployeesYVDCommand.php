<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays;

class GetEmployeesYVDCommand
{
    private int $year;

    /**
     * @param int $year
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): void
    {
        $this->year = $year;
    }
}
