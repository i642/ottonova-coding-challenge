<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays;

use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\EmployeeYVD;

interface GetEmployeesYVDUsecaseOutputPort
{
    /**
     * @param EmployeeYVD[] $employeeYVDs
     * @return array
     */
    public function present(array $employeeYVDs): array;
}
