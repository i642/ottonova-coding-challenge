<?php

namespace App\Unit\Usecase\GetEmployeeYearlyVacationDays\DTO;

use DateTimeImmutable;
use Ottonova\Infrastructure\Exceptions\InvalidContractStartDayException;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\ContractDTO;
use TestCase;

class ContractDTOTest extends TestCase
{
    /**
     * @dataProvider correctStartDayProvider
     * @throws \Exception
     */
    public function testShouldCreateContractDTOSuccessfully(string $startDay)
    {
        $date = new DateTimeImmutable($startDay . '.01.2001');
        $dummyVacationDays = 20;
        $cdrDTOBuilder = new ContractDTO($date, $dummyVacationDays);

        $expectedDay = (int)$startDay;
        self::assertEquals($expectedDay, $cdrDTOBuilder->getStartDay());
        self::assertEquals(1, $cdrDTOBuilder->getStartMonth());
        self::assertEquals(2001, $cdrDTOBuilder->getStartYear());
    }

    public function correctStartDayProvider(): array
    {
        return [
            ['startDay' => '01'],
            ['startDay' => '15'],
        ];
    }

    /**
     * @dataProvider incorrectStartDayProvider
     * @throws \Exception
     */
    public function testShouldThrowExceptionIfIncorrectStartDateProvided(string $startDay)
    {
        $date = new DateTimeImmutable($startDay . '.01.2001');
        $dummyVacationDays = 20;

        $this->expectException(InvalidContractStartDayException::class);
        $this->expectExceptionMessage(trans('exceptions.invalidContractStartDay'));
        new ContractDTO($date, $dummyVacationDays);
    }

    public function incorrectStartDayProvider(): array
    {
        return [
            ['startDay' => '02'],
            ['startDay' => '04'],
            ['startDay' => '08'],
            ['startDay' => '13'],
            ['startDay' => '14'],
            ['startDay' => '16'],
            ['startDay' => '20'],
            ['startDay' => '25'],
            ['startDay' => '30'],
        ];
    }
}
