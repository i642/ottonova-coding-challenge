<?php

namespace App\Unit\Usecase\GetEmployeeYearlyVacationDays\DTO;

use App\Unit\TestDoubles\EmployeeContractRecord\EmployeeContractBuilder;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTOTransformer;
use TestCase;

class EmployeeDTOTransformerTest extends TestCase
{

    public function testShouldTransformSuccessfully()
    {
        $employeeName = 'employee dummy name';
        $sut = new EmployeeDTOTransformer();

        $employeeContractRawRecord = (new EmployeeContractBuilder())->withName($employeeName)
            ->withBirthDate('24.07.1992')
            ->withContractStartDate('15.05.2018')
            ->withSpecialContractVacationDays(null)
            ->build();

        $actual = $sut->transform($employeeContractRawRecord);
        self::assertEquals($employeeName, $actual->getFullName());
        self::assertEquals(null, $actual->getContract()->getSpecialContractVacationDays());
    }
}
