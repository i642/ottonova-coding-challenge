<?php

namespace App\Unit\TestDoubles\EmployeeContractRecord;

use Faker\Factory;

class EmployeeContractBuilder
{
    private string $name;
    private string $birthDate;
    private string $contractStartDate;
    private ?int $specialContractVacationDays;

    public function __construct()
    {
        $faker = Factory::create();
        $this->name = $faker->name;
        $this->birthDate = '01.05.1980';
        $this->contractStartDate = '01.07.2018';
        $this->specialContractVacationDays = 27;
    }

    public function withName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function withBirthDate(string $birthDate): self
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    public function withContractStartDate(string $contractStartDate): self
    {
        $this->contractStartDate = $contractStartDate;
        return $this;
    }

    public function withSpecialContractVacationDays(?int $specialContractVacationDays): self
    {
        $this->specialContractVacationDays = $specialContractVacationDays;
        return $this;
    }

    public function build(): array
    {
        return [
            'name' => $this->name,
            'birth_date' => $this->birthDate,
            'contract_start_date' => $this->contractStartDate,
            'special_contract_vacation_days' => $this->specialContractVacationDays,
        ];
    }
}
