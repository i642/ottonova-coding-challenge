<?php

namespace Ottonova\Infrastructure\Services\CalculateEmployeeYVD;

class EmployeeYVD
{
    private string $fullName;
    private ?float $vacationDays;

    /**
     * @param string $fullName
     * @param float|null $vacationDays
     */
    public function __construct(string $fullName, ?float $vacationDays)
    {
        $this->fullName = $fullName;
        $this->setVacationDays($vacationDays);
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return float|null
     */
    public function getVacationDays(): ?float
    {
        return $this->vacationDays;
    }

    private function setVacationDays(?float $vacationDays)
    {
        $this->vacationDays = $vacationDays;
        if (null !== $vacationDays) {
            $this->vacationDays = number_format($vacationDays, 2, '.', '');
        }
    }
}
