<?php

namespace App\Unit\TestDoubles\GetEmployeeYearlyVacationDays;

use DateTimeImmutable;
use Faker\Factory;
use Ottonova\Infrastructure\Exceptions\InvalidContractStartDayException;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\ContractDTO;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTO;

class EmployeeDTOBuilder
{

    private string $fullName;
    private DateTimeImmutable $birthDate;
    private ContractDTO $contract;

    /**
     * @throws InvalidContractStartDayException
     */
    public function __construct()
    {
        $faker = Factory::create();
        $this->fullName = $faker->name;
        $this->birthDate = new DateTimeImmutable('01.05.1980');
        $this->contract = (new ContractDTOBuilder())->build();
    }

    public function withFullName(string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @param string $birthDate
     * @return $this
     * @throws \Exception
     */
    public function withBirthDate(string $birthDate): self
    {
        $this->birthDate = new DateTimeImmutable($birthDate);
        return $this;
    }

    public function withContract(ContractDTO $contractDTO): self
    {
        $this->contract = $contractDTO;
        return $this;
    }


    public function build(): EmployeeDTO
    {
        return new EmployeeDTO(
            $this->fullName,
            $this->birthDate,
            $this->contract
        );
    }
}
