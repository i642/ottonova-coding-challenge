<?php

namespace App\Unit\Usecase\GetEmployeeYearlyVacationDays;

use App\Unit\TestDoubles\EmployeeContractRecord\EmployeeContractBuilder;
use Mockery;
use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepositoryInterface;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\CalculateEmployeeYVDServiceInterface;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDCommand;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecase;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecaseOutputPort;
use TestCase;

class GetEmployeesYVFUsecaseInteractorTest extends TestCase
{


    public function testShouldGoThoughTheLogicFellowSuccessfully()
    {
        $getEmployeesYVDCommand = new GetEmployeesYVDCommand(2001);

        $employeeContractRawRecord = [(new EmployeeContractBuilder())->build()];

        $employeeContractsRepository = Mockery::mock(EmployeeContractsRepositoryInterface::class)
            ->shouldReceive('getAllEmployees')
            ->once()
            ->andReturn($employeeContractRawRecord)
            ->getMock();
        $this->app->instance(EmployeeContractsRepositoryInterface::class, $employeeContractsRepository);

        $calculateEmployeeYVDService = Mockery::mock(CalculateEmployeeYVDServiceInterface::class)
            ->shouldReceive('calculate')
            ->once()
            ->getMock();
        $this->app->instance(CalculateEmployeeYVDServiceInterface::class, $calculateEmployeeYVDService);

        $getEmployeesYVDUsecasePresenter = Mockery::mock(GetEmployeesYVDUsecaseOutputPort::class)
            ->shouldReceive('present')
            ->once()
            ->getMock();
        $this->app->instance(GetEmployeesYVDUsecaseOutputPort::class, $getEmployeesYVDUsecasePresenter);


        $sut = $this->app->make(GetEmployeesYVDUsecase::class);
        $sut->handle($getEmployeesYVDCommand);
    }
}
