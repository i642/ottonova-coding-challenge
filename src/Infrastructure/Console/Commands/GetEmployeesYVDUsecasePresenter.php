<?php

namespace Ottonova\Infrastructure\Console\Commands;

use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecaseOutputPort;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\EmployeeYVD;

class GetEmployeesYVDUsecasePresenter implements GetEmployeesYVDUsecaseOutputPort
{
    /**
     * @param EmployeeYVD[] $employeeYVDs
     * @return array
     */
    public function present(array $employeeYVDs): array
    {
        $result = [];
        foreach ($employeeYVDs as $employeeYVD) {
            $result[$employeeYVD->getFullName()] =  $employeeYVD->getVacationDays() ?? 'No Vacation Days For Year of Interest';
        }

        return $result;
    }
}
