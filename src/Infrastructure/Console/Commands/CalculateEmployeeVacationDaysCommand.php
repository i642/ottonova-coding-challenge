<?php

namespace Ottonova\Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDCommand;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecase;

class CalculateEmployeeVacationDaysCommand extends Command
{

    private GetEmployeesYVDUsecase $getEmployeesYVDUsecase;

    protected $signature = 'ottonova:employee:vacation-days:calculate {year}';

    protected $description = 'This command calculates the vacation days of employees for the year provided in input';

    /**
     * @param GetEmployeesYVDUsecase $getEmployeesYVDUsecase
     */
    public function __construct(GetEmployeesYVDUsecase $getEmployeesYVDUsecase)
    {
        parent::__construct();
        $this->getEmployeesYVDUsecase = $getEmployeesYVDUsecase;
    }

    public function handle()
    {
        $year = $this->argument('year');
        $employeesYVDs = $this->getEmployeesYVDUsecase->handle(new GetEmployeesYVDCommand($year));

        foreach ($employeesYVDs as $employeeName => $vacationDays) {
            $this->line("FullName: {$employeeName} : Vacation Days: {$vacationDays}");
        }
    }
}
