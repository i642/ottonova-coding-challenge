<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays;

interface GetEmployeesYVDUsecase
{
    public function handle(GetEmployeesYVDCommand $getEmployeeYearlyVacationDaysCommand):array;
}
