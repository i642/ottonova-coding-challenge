<?php

namespace App\Unit\Infrastructure\Services\CalculateEmployeeYVD;

use App\Unit\TestDoubles\GetEmployeeYearlyVacationDays\ContractDTOBuilder;
use App\Unit\TestDoubles\GetEmployeeYearlyVacationDays\EmployeeDTOBuilder;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\CalculateEmployeeYVDServiceInterface;
use TestCase;

class CalculateEmployeeYVDServiceTest extends TestCase
{
    public function testShouldSetYVDtoNullIfEmployeeHasNoContractInTheYearOfInterest()
    {
        $dummyYearOfInterest = 2009;
        $employeeDTO = (new EmployeeDTOBuilder())
            ->withContract((new ContractDTOBuilder())->withStartDate('01.08.2010')->build())
            ->build();
        $sut = $this->app->make(CalculateEmployeeYVDServiceInterface::class);
        $actual = $sut->calculate($dummyYearOfInterest, $employeeDTO);

        self::assertNull($actual->getVacationDays());
    }

    /**
     * @dataProvider sameInterestYearAndContractYearDataProvider
     * @throws \Exception
     */
    public function testShouldCalculateTheVacationDaysCorrectlyIfYearOfInterestIsSameAsContractStartYear(
        $contractStartDay,
        $contractStartMonth,
        $expectedVacationDays
    ) {
        $dummySameYearOfInterest = 2010;
        $employeeDTO = (new EmployeeDTOBuilder())
            ->withBirthDate('24.07.1995')
            ->withContract(
                (new ContractDTOBuilder())
                    ->withStartDate("{$contractStartDay}.{$contractStartMonth}.{$dummySameYearOfInterest}")
                    ->withSpecialContractVacationDays(null)
                    ->build()
            )
            ->build();
        $sut = $this->app->make(CalculateEmployeeYVDServiceInterface::class);
        $actual = $sut->calculate($dummySameYearOfInterest, $employeeDTO);
        self::assertEquals($expectedVacationDays, $actual->getVacationDays());
    }

    public function sameInterestYearAndContractYearDataProvider(): array
    {
        return [
            ['contractStartDay' => 01, 'contractStartMonth' => 01, 'expectedVacationDays' => 26,],
            ['contractStartDay' => 15, 'contractStartMonth' => 01, 'expectedVacationDays' => 24.92,],
            ['contractStartDay' => 01, 'contractStartMonth' => 02, 'expectedVacationDays' => 23.83,],
            ['contractStartDay' => 15, 'contractStartMonth' => 02, 'expectedVacationDays' => 22.75,],
            ['contractStartDay' => 15, 'contractStartMonth' => 11, 'expectedVacationDays' => 3.25,],
            ['contractStartDay' => 01, 'contractStartMonth' => 12, 'expectedVacationDays' => 2.17,],
            ['contractStartDay' => 15, 'contractStartMonth' => 12, 'expectedVacationDays' => 1.08,],
        ];
    }

    /**
     * @dataProvider sameInterestYearAndContractYearForSpecialContractDataProvider
     * @throws \Exception
     */
    public function testShouldOverRideTheSpecialContractVacationDaysAndCalculateTheSameYearCorrectly(
        $contractStartDay,
        $contractStartMonth,
        $expectedVacationDays
    ) {
        $dummySameYearOfInterest = 2010;
        $employeeDTO = (new EmployeeDTOBuilder())
            ->withBirthDate('24.07.1995')
            ->withContract(
                (new ContractDTOBuilder())
                    ->withStartDate("{$contractStartDay}.{$contractStartMonth}.{$dummySameYearOfInterest}")
                    ->withSpecialContractVacationDays(50)
                    ->build()
            )
            ->build();
        $sut = $this->app->make(CalculateEmployeeYVDServiceInterface::class);
        $actual = $sut->calculate($dummySameYearOfInterest, $employeeDTO);

        self::assertEquals($expectedVacationDays, $actual->getVacationDays());
    }

    public function sameInterestYearAndContractYearForSpecialContractDataProvider(): array
    {
        return [
            ['contractStartDay' => 01, 'contractStartMonth' => 01, 'expectedVacationDays' => 50,],
            ['contractStartDay' => 15, 'contractStartMonth' => 01, 'expectedVacationDays' => 47.92,],
            ['contractStartDay' => 01, 'contractStartMonth' => 02, 'expectedVacationDays' => 45.83,],
            ['contractStartDay' => 01, 'contractStartMonth' => 12, 'expectedVacationDays' => 4.17,],
            ['contractStartDay' => 15, 'contractStartMonth' => 12, 'expectedVacationDays' => 2.08,],
        ];
    }

    /**
     * @dataProvider contractStartYearProvider
     * @throws \Exception
     */
    public function testShouldShouldCalculateAdditionalVacationDaysBasedOnTheBirthDayOfEmployee(
        $yearOfInterest,
        $expectedVacationDays
    ) {
        $employeeDTO = (new EmployeeDTOBuilder())
            ->withBirthDate('24.07.1980')
            ->withContract(
                (new ContractDTOBuilder())
                    ->withStartDate("15.05.2005")
                    ->withSpecialContractVacationDays(26)
                    ->build()
            )
            ->build();
        $sut = $this->app->make(CalculateEmployeeYVDServiceInterface::class);
        $actual = $sut->calculate($yearOfInterest, $employeeDTO);

        self::assertEquals($expectedVacationDays, $actual->getVacationDays());
    }

    public function contractStartYearProvider(): array
    {
        return [
            ['yearOfInterest' => 2010,  'expectedVacationDays' => 26,],
            ['yearOfInterest' => 2011,  'expectedVacationDays' => 27,],
            ['yearOfInterest' => 2014,  'expectedVacationDays' => 27,],
            ['yearOfInterest' => 2016,  'expectedVacationDays' => 28,],
            ['yearOfInterest' => 2021,  'expectedVacationDays' => 29,],
            ['yearOfInterest' => 2030,  'expectedVacationDays' => 30,],
        ];
    }
}
