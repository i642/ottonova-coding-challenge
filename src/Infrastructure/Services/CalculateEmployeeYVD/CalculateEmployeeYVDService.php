<?php

namespace Ottonova\Infrastructure\Services\CalculateEmployeeYVD;

use DateTime;
use DateTimeImmutable;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTO;

class CalculateEmployeeYVDService implements CalculateEmployeeYVDServiceInterface
{
    const EMPLOYEE_YEARLY_MIN_VACATION_DAYS = 26;
    const ADDITIONAL_VACATION_AGE_LIMIT = 30;
    const ADDITIONAL_VACATION_PERIOD = 5;

    public function calculate(int $yearOfInterest, EmployeeDTO $employeeDTO): EmployeeYVD
    {
        $contractStartDate = $employeeDTO->getContract()->getStartDate();
        if (!$this->isYearOfInterestValidForContract($yearOfInterest, $employeeDTO->getContract()->getStartYear())) {
            return new EmployeeYVD($employeeDTO->getFullName(), null);
        }

        $yearlyVacationDays = self::EMPLOYEE_YEARLY_MIN_VACATION_DAYS;
        if (null !== $employeeDTO->getContract()->getSpecialContractVacationDays()) {
            $yearlyVacationDays = $employeeDTO->getContract()->getSpecialContractVacationDays();
        }

        if ($yearOfInterest === $employeeDTO->getContract()->getStartYear()) {
            $employmentMonthCount = $this->employmentMonthsCount(
                $employeeDTO->getContract()->getStartMonth(),
                $employeeDTO->getContract()->getStartDay()
            );
            $calculatedDays = $yearlyVacationDays * (1 / 12) * $employmentMonthCount;
            return new EmployeeYVD($employeeDTO->getFullName(), $calculatedDays);
        }

        $additionalVacationDays = $this->calculateAdditionalVacationDays(
            $yearOfInterest,
            $employeeDTO->getBirthDate(),
            $contractStartDate
        );
        $yearlyVacationDays += $additionalVacationDays;
        return new EmployeeYVD($employeeDTO->getFullName(), $yearlyVacationDays);
    }

    private function isYearOfInterestValidForContract(int $yearOfInterest, int $contractStartYear): bool
    {
        if ($yearOfInterest >= $contractStartYear) {
            return true;
        }

        return false;
    }

    private function calculateAdditionalVacationDays(
        int $yearOfInterest,
        DateTimeImmutable $employeeBirthDate,
        DateTimeImmutable $contractStartDate
    ): int {
        $yearOfInterestDateTime = new DateTime($yearOfInterest . '-01-01');

        $dateDiff = $employeeBirthDate->diff($yearOfInterestDateTime);
        $employeeAge = $dateDiff->y;

        if (self::ADDITIONAL_VACATION_AGE_LIMIT > $employeeAge) {
            return 0;
        }
        $dateDiff = $contractStartDate->diff($yearOfInterestDateTime);
        $employmentDuration = $dateDiff->y;
        return ($employmentDuration) / self::ADDITIONAL_VACATION_PERIOD;
    }

    private function employmentMonthsCount(int $contractStartMonth, int $contractStartDay): float
    {
        $employmentMonthCount = 12 - ($contractStartMonth - 1);
        if (15 === $contractStartDay) {
            $employmentMonthCount = $employmentMonthCount - 0.5;
        }
        return $employmentMonthCount;
    }
}
