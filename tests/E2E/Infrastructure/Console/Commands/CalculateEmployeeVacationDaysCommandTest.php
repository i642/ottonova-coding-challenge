<?php

namespace App\E2E\Infrastructure\Console\Commands;

use App\Unit\TestDoubles\EmployeeContractRecord\EmployeeContractBuilder;
use Illuminate\Support\Facades\Artisan;
use Ottonova\Infrastructure\Console\Commands\CalculateEmployeeVacationDaysCommand;
use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepositoryInterface;
use Mockery;
use TestCase;

class CalculateEmployeeVacationDaysCommandTest extends TestCase
{
    public function testShouldReturnNoVacationDayOutputIfEmployeeHasNoContractInTheYearOfInterest()
    {
        $employeeContractRawRecord = [(new EmployeeContractBuilder())
            ->withName('dummy Name')
            ->withBirthDate('26.05.1980')
            ->withContractStartDate('15.05.2010')
            ->withSpecialContractVacationDays(null)
            ->build()];

        $employeeContractsRepository = Mockery::mock(EmployeeContractsRepositoryInterface::class)
            ->shouldReceive('getAllEmployees')
            ->once()
            ->andReturn($employeeContractRawRecord)
            ->getMock();
        $this->app->instance(EmployeeContractsRepositoryInterface::class, $employeeContractsRepository);

        $this->app->make(CalculateEmployeeVacationDaysCommand::class);
        $this->artisan('ottonova:employee:vacation-days:calculate 2009');
        $actual = Artisan::output();

        $expected = 'FullName: dummy Name : Vacation Days: No Vacation Days For Year of Interest';
        self::assertEquals($expected , trim($actual));
    }

    public function testShouldOutPutExpectedResultSuccessfully()
    {
        $employeeContractRawRecord = [(new EmployeeContractBuilder())
            ->withName('dummy Name')
            ->withBirthDate('26.05.1980')
            ->withContractStartDate('15.05.2010')
            ->withSpecialContractVacationDays(null)
            ->build()];

        $employeeContractsRepository = Mockery::mock(EmployeeContractsRepositoryInterface::class)
            ->shouldReceive('getAllEmployees')
            ->once()
            ->andReturn($employeeContractRawRecord)
            ->getMock();
        $this->app->instance(EmployeeContractsRepositoryInterface::class, $employeeContractsRepository);

        $this->app->make(CalculateEmployeeVacationDaysCommand::class);
        $this->artisan('ottonova:employee:vacation-days:calculate 2021');
        $actual = Artisan::output();
        $expected = 'FullName: dummy Name : Vacation Days: 28';
        self::assertEquals($expected , trim($actual));
    }
}
