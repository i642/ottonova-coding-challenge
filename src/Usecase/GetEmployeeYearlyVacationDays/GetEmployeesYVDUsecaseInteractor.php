<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays;

use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepositoryInterface;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\CalculateEmployeeYVDServiceInterface;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTO;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTOTransformer;

class GetEmployeesYVDUsecaseInteractor implements GetEmployeesYVDUsecase
{
    private EmployeeContractsRepositoryInterface $employeeContractsRepository;
    private CalculateEmployeeYVDServiceInterface $calculateEmployeeYVDService;
    private EmployeeDTOTransformer $employeeDTOTransformer;
    private GetEmployeesYVDUsecaseOutputPort $getEmployeesYVDUsecaseOutputPort;

    /**
     * @param EmployeeContractsRepositoryInterface $employeeRepository
     * @param CalculateEmployeeYVDServiceInterface $calculateEmployeeYVDService
     */
    public function __construct(
        EmployeeContractsRepositoryInterface $employeeRepository,
        CalculateEmployeeYVDServiceInterface $calculateEmployeeYVDService,
        EmployeeDTOTransformer $employeeDTOTransformer,
        GetEmployeesYVDUsecaseOutputPort $getEmployeesYVDUsecaseOutputPort
    ) {
        $this->employeeContractsRepository = $employeeRepository;
        $this->calculateEmployeeYVDService = $calculateEmployeeYVDService;
        $this->employeeDTOTransformer = $employeeDTOTransformer;
        $this->getEmployeesYVDUsecaseOutputPort = $getEmployeesYVDUsecaseOutputPort;
    }

    /**
     * @param GetEmployeesYVDCommand $getEmployeeYearlyVacationDaysCommand
     * @return array
     * @throws \Exception
     */
    public function handle(GetEmployeesYVDCommand $getEmployeeYearlyVacationDaysCommand): array
    {
        $yearOfInterest = $getEmployeeYearlyVacationDaysCommand->getYear();

        $employeeContracts = $this->employeeContractsRepository->getAllEmployees();

        $transformedEmployees = $this->transformToEmployeeDTOs($employeeContracts);
        $employeeYVDs = $this->calculateEmployeeYVDs($yearOfInterest, $transformedEmployees);
        return $this->getEmployeesYVDUsecaseOutputPort->present($employeeYVDs);
    }

    /**
     * @param array $employeeContracts
     * @return EmployeeDTO[]
     * @throws \Exception
     */
    private function transformToEmployeeDTOs(array $employeeContracts): array
    {
        $transformedEmployees = [];
        foreach ($employeeContracts as $employee) {
            $transformedEmployees[] = $this->employeeDTOTransformer->transform($employee);
        }
        return $transformedEmployees;
    }

    /**
     * @param int $yearOfInterest
     * @param EmployeeDTO[] $transformedEmployees
     * @return array
     */
    private function calculateEmployeeYVDs(int $yearOfInterest, array $transformedEmployees): array
    {
        $employeesWithYVDs = [];
        foreach ($transformedEmployees as $employeeDTO) {
            $employeesWithYVDs[] = $this->calculateEmployeeYVDService->calculate($yearOfInterest, $employeeDTO);
        }

        return $employeesWithYVDs;
    }
}
