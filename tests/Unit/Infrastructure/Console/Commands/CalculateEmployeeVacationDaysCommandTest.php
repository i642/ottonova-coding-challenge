<?php

namespace App\Unit\Infrastructure\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Ottonova\Infrastructure\Console\Commands\CalculateEmployeeVacationDaysCommand;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecase;
use Mockery;
use TestCase;

class CalculateEmployeeVacationDaysCommandTest extends TestCase
{
    public function testShouldRunTheCalculateEmployeeYVDCommandAndOutPutSuccessfully()
    {
        $getEmployeesYVDUsecase = Mockery::mock(GetEmployeesYVDUsecase::class)
            ->shouldReceive('handle')
            ->once()
            ->andReturn(['dummyName' => 27])
            ->getMock();
        $this->app->instance(GetEmployeesYVDUsecase::class, $getEmployeesYVDUsecase);

        $this->app->make(CalculateEmployeeVacationDaysCommand::class);
        $this->artisan('ottonova:employee:vacation-days:calculate 2009');
        $actual = Artisan::output();
        self::assertEquals('FullName: dummyName : Vacation Days: 27' , trim($actual));
    }
}
