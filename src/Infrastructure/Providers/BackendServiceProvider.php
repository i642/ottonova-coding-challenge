<?php

namespace Ottonova\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Ottonova\Infrastructure\Console\Commands\GetEmployeesYVDUsecasePresenter;
use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepository;
use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepositoryInterface;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\CalculateEmployeeYVDService;
use Ottonova\Infrastructure\Services\CalculateEmployeeYVD\CalculateEmployeeYVDServiceInterface;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecase;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecaseInteractor;
use Ottonova\Usecase\GetEmployeeYearlyVacationDays\GetEmployeesYVDUsecaseOutputPort;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EmployeeContractsRepositoryInterface::class, EmployeeContractsRepository::class);
        $this->app->bind(GetEmployeesYVDUsecase::class, GetEmployeesYVDUsecaseInteractor::class);
        $this->app->bind(CalculateEmployeeYVDServiceInterface::class, CalculateEmployeeYVDService::class);
        $this->app->bind(GetEmployeesYVDUsecaseOutputPort::class, GetEmployeesYVDUsecasePresenter::class);
    }
}
