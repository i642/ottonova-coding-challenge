<?php

namespace App\Integration\Infrastructure\Persistence\Repository\Employee;

use Ottonova\Infrastructure\Persistence\Repository\Employee\EmployeeContractsRepositoryInterface;
use TestCase;

class EmployeeContractsRepositoryTest extends TestCase
{
    public function testShouldFetchEmployeeContractsDataWithCorrectFormat()
    {
        $sut = $this->app->make(EmployeeContractsRepositoryInterface::class);
        $employeeContracts = $sut->getAllEmployees();
        self::assertGreaterThan(0, count($employeeContracts));
        self::arrayHasKey($employeeContracts[0]['name']);
        self::arrayHasKey($employeeContracts[0]['birth_date']);
        self::arrayHasKey($employeeContracts[0]['contract_start_date']);
        self::arrayHasKey($employeeContracts[0]['special_contract_vacation_days']);
    }
}
