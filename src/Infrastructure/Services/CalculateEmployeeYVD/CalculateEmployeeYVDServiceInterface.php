<?php

namespace Ottonova\Infrastructure\Services\CalculateEmployeeYVD;

use Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO\EmployeeDTO;

interface CalculateEmployeeYVDServiceInterface
{
    public function calculate(int $yearOfInterest , EmployeeDTO $employeeDTO);
}
