# Ottonova Calculate Employee Annually Vacation Days
(Coding Challenge)

This project is an implementation of Calculating Employee Annually Vacation Days based on the provided year of interest.

## Environment Setup

Please navigate to the root of the project and run the following command to build the docker containers:

-`docker-compose up --build`

After the first time the docker containers are build you can run the following command to start the containers:

-`docker-compose up -d`

or

-`docker-compose start`

The app should be up and running on the following URL:

http://localhost:500/

## How run Command:

You can run the php artisan command to calculate the employee annually vacation days following the steps below:

* `docker-compose up -d`
* `docker exec -it ottonova-php sh`
* `php artisan ottonova:employee:vacation-days:calculate {year}`
  * e.g. `php artisan ottonova:employee:vacation-days:calculate 2016`


## Running Tests
You can simply run the tests by PHPUnit following below steps:
* `docker-compose up -d`
* `docker exec -it ottonova-php sh`
* `vendor/bin/phpunit`

Or If you are using the PhpStorm as your IDE follow the following steps to run the test suit:

* Go to setting by pressing **Ctrl+Alt+S** keys
* Go to **PHP** section
* Select 7.4 as The **PHP Language Level**
* Click on **...** in front of the **CLI Interpreter**:
    * Click on **+** Button
        * Select **From Docker, Vagrant,...**
        * Select **Docker Compose**
        * Select **Docker** as Server
        * Select **php** as Service
        * Click **OK**
    * You should be able to see the **PHP Version** in the **General** section of **CLI Interpreter**
    * Click **OK**
* Click **OK** for the last time

Now on the toolbar please expand the drop-down list next to Run tests button
* Select **phpunit.xml** and the click on **Edit Configurations...**
* Select the last radio button: **Defined in the configuration file**
* Then in the **Command Line** section make sure that the **php (7.4.23)** is selected as **Interpreter**
* Click **Ok**

Finally, click on the green play button to run the tests.

**_Hope you enjoy :)_**


**Sina Fetrat**
