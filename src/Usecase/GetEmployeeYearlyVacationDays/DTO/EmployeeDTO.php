<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO;

use DateTimeImmutable;

class EmployeeDTO
{
    private string $fullName;
    private DateTimeImmutable $birthDate;
    private ContractDTO $contract;

    /**
     * @param string $fullName
     * @param DateTimeImmutable $birthDate
     * @param ContractDTO $contract
     */
    public function __construct(string $fullName, DateTimeImmutable $birthDate, ContractDTO $contract)
    {
        $this->fullName = $fullName;
        $this->birthDate = $birthDate;
        $this->contract = $contract;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBirthDate(): DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @return ContractDTO
     */
    public function getContract(): ContractDTO
    {
        return $this->contract;
    }
}
