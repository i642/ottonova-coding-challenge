<?php

namespace Ottonova\Usecase\GetEmployeeYearlyVacationDays\DTO;

use DateTimeImmutable;

class EmployeeDTOTransformer
{
    /**
     * @param array $employee
     * @return EmployeeDTO
     * @throws \Exception
     */
    public function transform(array $employee): EmployeeDTO
    {
        $employeeFullName = $employee['name'];
        $employeeBirthDate = new DateTimeImmutable($employee['birth_date']);
        $contractStartDate = new DateTimeImmutable($employee['contract_start_date']);
        $specialContractVacationDays = $employee['special_contract_vacation_days'];

        $contractDTO = new ContractDTO($contractStartDate, $specialContractVacationDays);
        return new EmployeeDTO($employeeFullName, $employeeBirthDate, $contractDTO);
    }
}
